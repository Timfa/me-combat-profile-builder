const CLASSES = 
{
    FIGHTER: "Fighter",
    BRUISER: "Bruiser",
    DASHER: "Dasher",
    MYSTIC: "Mystic",
    SAVANT: "Savant",
    ORACLE: "Oracle"
}

var character = 
{
    name: "Character Name",
    stats:
    {
        STR: 8,
        DEX: 8,
        CON: 8,
        INT: 8,
        CHA: 8
    },
    OC: false,
    class: CLASSES.FIGHTER,
    rank: "C"
}

function init()
{
    regenerateCommand();
    regenerateBar("STR");
    regenerateBar("DEX");
    regenerateBar("CON");
    regenerateBar("INT");
    regenerateBar("CHA");
}

function onNameChange()
{
    character.name = document.getElementById("name").value.replace(",", "")
    document.getElementById("name").value = character.name;

    regenerateCommand();
}

function setStat(stat)
{
    console.log("Setting " + stat)
    character.stats[stat] = document.getElementById(stat.toLowerCase() + "Num").value;

    character.stats[stat] = Math.min(20, Math.max(1, character.stats[stat]));  
    document.getElementById(stat.toLowerCase() + "Num").value = character.stats[stat]

    regenerateBar(stat)
    regenerateCommand();
}

function reduceStat(stat)
{
    character.stats[stat]--;

    character.stats[stat] = Math.min(20, Math.max(1, character.stats[stat]));
    document.getElementById(stat.toLowerCase() + "Num").value = character.stats[stat]
    
    regenerateBar(stat)
    regenerateCommand();
}

function increaseStat(stat)
{
    character.stats[stat]++;

    character.stats[stat] = Math.min(20, Math.max(1, character.stats[stat]));
    document.getElementById(stat.toLowerCase() + "Num").value = character.stats[stat]

    regenerateBar(stat)
    regenerateCommand();
}

function regenerateBar(stat)
{
    document.getElementById(stat.toLowerCase() + "Bar").innerText = "";
    let remaining = 20;

    for(let i = 0; i < character.stats[stat]; i++)
    {
        remaining--;
        document.getElementById(stat.toLowerCase() + "Bar").innerText += String.fromCharCode(9670);
    }
    
    for(let i = 0; i < remaining; i++)
    {
        document.getElementById(stat.toLowerCase() + "Bar").innerText += String.fromCharCode(9671);
    }
}

function setClass()
{
    character.class = document.getElementById("class").value;
    regenerateCommand();
}

function setCanon()
{
    character.OC = document.getElementById("canon").value == "OC";

    if(character.OC && character.rank == "A")
    {
        character.rank = "B";
        document.getElementById("rank").value = "B"
    }

    regenerateCommand();
}

function setRank()
{
    character.rank = document.getElementById("rank").value;

    if(character.OC && character.rank == "A")
    {
        character.rank = "B";
        document.getElementById("rank").value = "B"
    }

    regenerateCommand();
}

function regenerateCommand()
{
    let spent = character.stats.STR + character.stats.DEX + character.stats.CON + character.stats.INT + character.stats.CHA 
    
    let remaining = 0;

    switch(character.rank)
    {
        case "C": remaining = 50 - spent; break;
        case "B": remaining = 55 - spent; break;
        case "A": remaining = 60 - spent; break;
    }

    document.getElementById("spent").innerText = spent;
    document.getElementById("remaining").innerText = remaining;

    if(remaining < 0)
        document.getElementById("outputCommand").value = "Character has too many stat points assigned!";
    else
        document.getElementById("outputCommand").value = "%qregister " + character.name + ", " 
        + character.stats.STR + ", "
        + character.stats.DEX + ", "
        + character.stats.CON + ", "
        + character.stats.INT + ", "
        + character.stats.CHA + ", "
        + character.class;

    console.log("Regenerated")
}

onload = init;

console.log("◇".charCodeAt(0))